import math as math
import numpy as np
import src.Vector2d as V2d


class Point:
    def __init__(self):
        pass

    def InMatrix(self, A: V2d):
        p_matrix = np.zeros((1, 2), float)
        p_matrix[0][0] = V2d.Vector2d.X(A)
        p_matrix[0][1] = V2d.Vector2d.Y(A)
        return p_matrix

    def Distance(self, A: V2d, B: V2d):
        distance = math.sqrt(math.pow((A.x - B.x), 2) + math.pow((A.y - B.y), 2))
        return distance

    def ReallyDif(self, cont1: int, cont2: int, mat1: np.matrix, mat2: np.matrix):
        pass

    def SearchDif(self, cont1: int, cont2: int, indice: int, mat1: np.matrix, mat2: np.matrix, mat3: np.matrix, mat4: np.matrix):
        pass

    def NewPoints(self, vertices, intersection_points, s1: V2d, s2: V2d, num_vertex: int, num_intersections: int):
        pass

'''
Classe Punto -> FUNZIONA
1. Metodo cotruttore: vuoto
2. Metodo InMatrix: prende un V2d e lo porta in np.matrix
3. Metodo Distance: prende due V2d e ne calcola la distanza
4. Metodo ReallyDif: prende due matrici e calcola quanti elementi della seconda matrice sono diversi dalla prima
5. Metodo SearchDif: prende quattro matrici e riempie la terza (Serve per creare la nuova matrice solo con i punti interessanti)
6. Metodo NewPoints: prende i vari punti e resituisce la matrice dei nuovi punti con vertici, intersezioni e estremo del segmento

-Tengo conto della tolleranza
'''
