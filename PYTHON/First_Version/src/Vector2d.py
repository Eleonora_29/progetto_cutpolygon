class Vector2d:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def X(self):
        return self.x

    def Y(self):
        return self.y


'''
Classe Vettore a due dimensioni -> FUNZIONA
1. Metodo costruttore: prende in input x e y e le assegna
2. Metodo X: resituisce il valore dell'ascissa
3. Metodo Y: restituisce il valore dell'ordinata
'''