import src.Vector2d as V2d
import src.Point as P
import numpy as np

class Segment:
    def __init__(self, s1: V2d, s2:V2d):
        self.s1 = s1
        self.s2 = s2

    def InMatrix(self, s1: V2d, s2: V2d):
        s1_matrix = P.Point.InMatrix(P, s1)
        s2_matrix = P.Point.InMatrix(P, s2)
        segment = np.concatenate((s1_matrix, s2_matrix), axis=0)

    def CoefAng(self, s1: V2d, s2:V2d):
        m_y = V2d.Vector2d.Y(s2) - V2d.Vector2d.Y(s1)
        m_x = V2d.Vector2d.X(s2) - V2d.Vector2d.X(s1)
        tol = 1e-6
        if (-tol <= m_x <= tol) and (m_y <= -tol or m_y >= tol):
            return 'Inf'
        elif (-tol <= m_y <= tol) and (m_x <= -tol or m_x >= tol):
            return 0
        elif (-tol <= m_x <= tol) and (-tol <= m_y <= tol):
            return 'I due punti passati coincidono'
        else:
            return m_y / m_x



'''
Classe Segmento ->FUNZIONA
1. Metodo costruttore: prende in input i V2d s1 e s2 e li inserisce in Segment(s1, s2)
2. Metodo InMatrix: prende i V2d s1 e s2 e li trasforma in una unica matrice contentente s1 e s2
3. Metodo Coef: prende in input s1 e s2 e restiuisce il coefficiente della retta che passa per i due V2d
            -Questo metodo tiene conto dei casi particolari di rette

-Tengo conto della tolleranza
'''

