from unittest import TestCase
import src.Point as Point
import src.Vector2d as V2d


class TestPoint(TestCase):
    def test_InMatrix(self):
        vector1 = V2d.Vector2d(1,2)
        point = Point.Point()
        result = str(Point.Point.InMatrix(point, vector1))
        self.assertEqual(result, "[[1. 2.]]")

    def test_Distance(self):
        vector1 = V2d.Vector2d(1, 2)
        vector2 = V2d.Vector2d(2, 3)
        point = Point.Point()
        self.assertTrue(abs(Point.Point.Distance(point, vector1, vector2) - 1.4142135) < 1e-6)

    def test_ReallyDiff(self):
        pass

    def test_SearchDif(self):
        pass

    def test_NewPoints(self):
        pass

'''
Classe di TestPoint -> FUNZIONA
1. Test su InMatrix: da V2d a np.matrix
2. Test su Distance: distanza tra due V2d
3. Test su ReallyDiff: conta quanti punti sono differenti tra due matrici
4. Test su SearchDif: costruisce una matrice nuova a partire da mat1 con gli elementi nuovi delle altre
5. Test su NewPoints: restiuisce i punti 

-Tengo conto della tolleranza
'''