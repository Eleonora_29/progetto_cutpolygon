#ifndef NEWPOINTS_H
#define NEWPOINTS_H

#include <iostream>
#include <Eigen>
#include <vector>

#include <Pol.hpp>

using namespace std;
using namespace Eigen;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class NewPoints: public INewPoints {

    public:
      static double tolerance;

    private:
      vector<Vector2d> _newPoints;
      vector<Vector2d> _vertex;
      vector<Vector2d> _intersectionPoints;
      Vector2d _s1, _s2;

      void Reset();

    public:
      NewPoints(const vector<Vector2d> vertex,
                const vector<Vector2d> intersectionPoints,
                const Vector2d s1, const Vector2d s2);

      vector<Vector2d> OrderedVertex(); //ordino i vertici che ricevo in input, in modo che il primo sia quello in basso a sinistra
      void CalcPoints ();
      vector<Vector2d> ReturnPoints () const {return _newPoints;} //contiene prima i vertici e poi le intersezioni
      int NumNewPoints () const {return _newPoints.size();}
  };
}

#endif // NEWPOINTS_H
