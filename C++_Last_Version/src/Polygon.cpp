#include "Polygon.hpp"
#include <math.h>

namespace PolygonCutLibrary {

void Polygon::Reset()
{
    _boundingBox.clear();
    _area = 0.0;
}

Polygon::Polygon(vector<Vector2d> vertex)
{
    _vertex = vertex;
}

double Polygon::ComputeArea()
{
    Reset();
    int numVertex = _vertex.size();
    int j;
    for (int i=0; i<numVertex; i++)
    {
        j= (i+1) % numVertex;
        _area += 0.5 * (_vertex[i][0]*_vertex[j][1] - _vertex[j][0]*_vertex[i][1]);
    }
    return _area;
}

vector<Vector2d> Polygon::ComputeBoundingBox()
{
    Reset();
    Vector2d p1, p2, p3, p4;
    double minX = _vertex[0][0], minY = _vertex[0][1];
    double maxX = _vertex[0][0], maxY = _vertex[0][1];
    // cerco le minime coordinate dei vertici, per creare la bounding box
    for (unsigned int i=1; i<_vertex.size(); i++)
    {
        if (_vertex[i][0] < minX)
            minX = _vertex[i][0];
        else if (_vertex[i][0] > maxX)
            maxX = _vertex[i][0];
        if (_vertex[i][1] < minY)
            minY = _vertex[i][1];
        else if (_vertex[i][1] > maxY)
            maxY = _vertex[i][1];
    }
    // salvo i 4 punti della bounding box in senso antiorario
    p1 << minX, minY;
    p2 << maxX, minY;
    p3 << maxX, maxY;
    p4 << minX, maxY;
    _boundingBox.push_back(p1);
    _boundingBox.push_back(p2);
    _boundingBox.push_back(p3);
    _boundingBox.push_back(p4);
    return _boundingBox;
}

}
