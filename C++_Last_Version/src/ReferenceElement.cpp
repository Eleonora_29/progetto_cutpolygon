#include "ReferenceElement.hpp"
#include <math.h>

namespace PolygonCutLibrary {

void ReferenceElement::Reset()
{
    _newBB.clear();
    _refElement.clear();
    _transElement.clear();
    _mesh.clear();
    _baseBB = 0;
    _heightBB = 0;
}

ReferenceElement::ReferenceElement(vector<Vector2d> polygon, vector<Vector2d> boundingBox)
{
    _polygon = polygon;
    _boundingBox = boundingBox;
}

vector<Vector2d> ReferenceElement::NewBoundingBox()
{
    Reset();
    Segment base(_boundingBox[0], _boundingBox[1]);
    _baseBB = base.Distance(); // lunghezza della base della bounding box
    Segment height(_boundingBox[1], _boundingBox[2]);
    _heightBB = height.Distance(); // altezza della bb

    double minX, minY, maxX, maxY;
    Vector2d p1, p2, p3, p4;
    minX = _boundingBox[0][0];
    minY = _boundingBox[0][1];
    maxX = _boundingBox[2][0];
    maxY = _boundingBox[2][1];
    // cerco i vertici del poligono che giacciono sul bordo della bounding box e li proietto sul lato opposto, creando i nuovi punti che mi servono per la mesh
    for (unsigned int i=0; i<_polygon.size(); i++)
    {
        if (_polygon[i][0] == minX)
            p2 << maxX, _polygon[i][1];
        if (_polygon[i][0] == maxX)
            p4 << minX, _polygon[i][1];
        if (_polygon[i][1] == minY)
            p3 << _polygon[i][0], maxY;
        if (_polygon[i][1] == maxY)
            p1 << _polygon[i][0], minY;
    }
    // li inserisco in modo che i punti della bb siano ordinati in senso antiorario
    _newBB.push_back(_boundingBox[0]);
    _newBB.push_back(p1);
    _newBB.push_back(_boundingBox[1]);
    _newBB.push_back(p2);
    _newBB.push_back(_boundingBox[2]);
    _newBB.push_back(p3);
    _newBB.push_back(_boundingBox[3]);
    _newBB.push_back(p4);

    return _newBB;
}

vector<Vector2d> ReferenceElement::CreateRefElement()
{
    //lo creo inserendo prima tutti i punti del poligono e poi i punti di newBB
    for (unsigned int i=0; i<_polygon.size(); i++)
        _refElement.push_back(_polygon[i]);
    for (unsigned int j=0; j<_newBB.size(); j++)
        _refElement.push_back(_newBB[j]);
    return _refElement;
}

vector<Vector2d> ReferenceElement::Translate(vector<Vector2d> element, Vector2d translation)
{
    _transElement.clear();
    for (unsigned int j=0; j<element.size(); j++)
        _transElement.push_back(element[j]+translation);

    return _transElement;
}

vector<vector<Vector2d> > ReferenceElement::CreateMesh(vector<Vector2d> domain)
{
    // la mesh è costruita come una serie di elementi, ciascuno contenente prima tutti i vertici del poligono inscritto, e poi i punti della bounding box (compresi i punti aggiunti)
    // è costruita dal basso verso l'alto, partendo dal basso a sinistra e procedendo per righe

    double l1, l2;
    Segment segm1(domain[0], domain[1]);
    l1 = segm1.Distance();
    Segment segm2(domain[1], domain[2]);
    l2 = segm2.Distance();

    double filledBase = 0; //spazio occupato sull'orizzontale
    double filledHeight = 0; //verticale
    Vector2d t, s, y;
    t << _baseBB, 0; // serve per traslare in orizzontale
    s << 0, _heightBB; //trasla in verticale
    int polAggiunti = 0;
    int numPolVertex;
    vector<Vector2d> pol, ultimoAggiunto, temp, bb;
    vector<vector<Vector2d>> poligoni; //salvo i tre poligoni in basso, senza bounding box

    for (int i=0; filledBase < l1; i++)
    {
        if (filledBase <= (l1 - _baseBB)) //vuol dire che ce ne sta ancora uno in orizzontale
        {
            if (polAggiunti == 0) //se è il primo che metto, aggiungo semplicemente l'elemento di riferimento
            {
                _mesh.push_back(_refElement);
                _ref.push_back(_newBB); //contiene solo le bounding box. Mi serve per calcolare le aree finali
                polAggiunti++;
                poligoni.push_back(_polygon);
                temp.clear();
                filledBase = _baseBB;
                ultimoAggiunto = _refElement; //mi segno qual è l'ultimo poligono aggiunto in modo da usarlo per la traslazione successiva
            }
            else
            {
                pol.clear();
                pol = Translate(ultimoAggiunto, t);
                _mesh.push_back(pol);
                for (unsigned int b=_polygon.size(); b<pol.size(); b++) //inserisco tutti i punti della bounding box di questo elemento
                   bb.push_back(pol[b]);
                _ref.push_back(bb);
                bb.clear();
                for (unsigned int a=0; a<_polygon.size(); a++) //qui inserisco solo i punti del poligono
                    temp.push_back(_mesh[i][a]);
                poligoni.push_back(temp);
                temp.clear();
                ultimoAggiunto.clear();
                ultimoAggiunto = pol;
                polAggiunti++;
                filledBase = filledBase + _baseBB; //mi segno che la porzione di base riempita dai poligoni è aumentata
            }
        }
        else //se non ci stanno più poligoni interi in orizzontale
        {
            pol.clear();
            Vector2d s1, s2;
            s1 = domain[1] + s/4;
            s2 = domain[1] + s/2;
            vector<Vector2d> el;
            el = Translate(_polygon, polAggiunti*t); //traslo il poligono di partenza
            CutPolygon element(el, s1, s2); //lo taglio, senza bounding box
            element.Cut();
            vector<Vector2d> intersectionPoints;
            intersectionPoints = element.IntersectionPoints();
            NewPoints newP(el, intersectionPoints, s1, s2);
            newP.CalcPoints();
            element.ChooseFunction();
            vector<vector<Vector2d>> cutted;
            cutted = element.CuttedPolygons(); //mi interessa solo il primo poligono che si forma (di sinistra)
            pol = cutted[0];
            // dopo il poligono tagliato devo aggiungere anche i punti della bounding box
            // inserisco tutti i vertici della bb. I nuovi punti invece li inserisco solo se stanno tra i vertici e non sono fuori
            // vertici e nuovi punti sono inseriti in ordine antiorario
            pol.push_back(domain[0] + polAggiunti*t);
            Vector2d t1, t2, t3, t4;
            t1 = _newBB[1] + polAggiunti*t;
            t2 = _newBB[3] + polAggiunti*t;
            t3 = _newBB[5] + polAggiunti*t;
            t4 = _newBB[7] + polAggiunti*t;
            if (t1[0] < domain[1][0])
                pol.push_back(t1);
            pol.push_back(domain[1]);
            pol.push_back(domain[1] + s);
            if (t3[0] < domain[1][0])
                pol.push_back(t3);
            pol.push_back(domain[0] + polAggiunti*t + s);
            pol.push_back(t4);
            _mesh.push_back(pol);
            numPolVertex = cutted[0].size(); //numero dei vertici del poligono che considero
            for (unsigned int b=numPolVertex; b<pol.size(); b++)
               bb.push_back(pol[b]); //questa è solo la bounding box con tutti i punti
            _ref.push_back(bb);
            bb.clear();
            for (unsigned int a=0; a<_polygon.size(); a++)
                temp.push_back(_mesh[i][a]); //questo è solo il poligono senza bounding box
            poligoni.push_back(temp);
            temp.clear();
            polAggiunti++;
            filledBase = l1; //il lato orizzontale è completo
        }
    }
    pol.clear();
    ultimoAggiunto.clear();
    filledHeight = _heightBB; // una riga è riempita
    int riga = 0; //indica l'ultima riga riempita
    int n = _mesh.size();
    for (int j=0; filledHeight < l2; j++) // se non è già tutto pieno
    {
        int z = riga*n;
        if (filledHeight <= (l2 - _heightBB)) //vuol dire che ce ne sta ancora uno in verticale
        {
            for (int k=z; k<(z+n); k++) //traslo in alto tutta la prima striscia
            {
                pol = Translate(_mesh[k], s);
                _mesh.push_back(pol);
                if (k != (z+n-1)) //se non è l'ultimo elemento della riga
                {
                    //so che il numero dei vertici è quello del poligono di partenza e parto da lì per salvarmi la bounding box
                    for (unsigned int b=_polygon.size(); b<pol.size(); b++)
                        bb.push_back(pol[b]);
                }
                else
                {
                    //altrimenti il numero dei vertici è quello del poligono tagliato, quindi parto da lì
                    for (unsigned int b=numPolVertex; b<pol.size(); b++)
                       bb.push_back(pol[b]);
                }
                _ref.push_back(bb);
                bb.clear();
                polAggiunti++;
            }
            filledHeight = filledHeight + _heightBB; //un'altra riga è riempita
            riga++;
            pol.clear();
        }
        else
        {
            int i =0;
            for (int k=z; k<(z+n); k++)
            {
                pol.clear();
                Vector2d s1, s2;
                //fisso degli estremi per il taglio, in modo che siano dentro il poligono
                s1 << (k-z)*_baseBB + 2, l2+1;
                s2 << (k-z)*_baseBB + 3, l2+1;
                vector<Vector2d> el;
                el = Translate(poligoni[i], (riga+1)*s); //traslo in alto di un'altezza corrispondente alle righe riempite
                CutPolygon element(el, s1, s2);
                element.Cut();
                vector<Vector2d> intersectionPoints;
                intersectionPoints = element.IntersectionPoints();
                NewPoints newP(el, intersectionPoints, s1, s2);
                newP.CalcPoints();
                element.ChooseFunction();
                pol = element.CuttedPolygons()[0];
                numPolVertex = element.CuttedPolygons()[0].size();
                // dopo il poligono tagliato devo aggiungere anche i punti della bounding box
                // inserisco tutti i vertici della bb. I nuovi punti invece li inserisco solo se stanno tra i vertici e non sono fuori
                // vertici e nuovi punti sono inseriti in ordine antiorario
                pol.push_back(domain[0] + 2*s + i*t);
                Vector2d t1, t2, t3, t4, t5, t6, t7;
                t1 = _newBB[1] + (riga+1)*s + i*t;
                t2 = _newBB[3] + (riga+1)*s + i*t;
                t3 = _newBB[5] + (riga+1)*s + i*t;
                t4 = _newBB[7] + (riga+1)*s + i*t;
                t5 = _newBB[2] + (riga+1)*s + i*t;
                t6 = domain[3] + (i+1)*t;
                t7 = domain[3] + i*t;
                pol.push_back(t1);
                if (t5[0]<domain[1][0])
                {
                    pol.push_back(t5);
                    if (t2[1] < domain[2][1])
                        pol.push_back(t2);
                    pol.push_back(t6);
                }
                else
                {
                    pol.push_back(domain[1] + 2*s);
                    pol.push_back(domain[2]);
                }
                pol.push_back(t7);
                if (t4[1] < domain[2][1])
                    pol.push_back(t4);
                _mesh.push_back(pol);
                for (unsigned int b=numPolVertex; b<pol.size(); b++)
                   bb.push_back(pol[b]);
                _ref.push_back(bb);
                bb.clear();
                polAggiunti++;
                i++;
            }
            filledHeight = l2; //il dominio è pieno anche in altezza, quindi ho finito
        }
    }
    return _mesh;
}

double ReferenceElement::AreaTot()
{
    // calcolo l'area totale della mesh, sommando quelle dei vari elementi
    _areaTot = 0;
    double areaEl = 0;
    for (unsigned int i=0; i<_ref.size(); i++)
    {
        Polygon element(_ref[i]);
        areaEl = element.ComputeArea();
        _areaTot += areaEl;
    }
    return _areaTot;
}

}
