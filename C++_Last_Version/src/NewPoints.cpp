#include <NewPoints.hpp>
#include <math.h>

namespace PolygonCutLibrary {

double NewPoints::tolerance = 1.0E-7;

void NewPoints::Reset()
{
    _newPoints.clear();
}

NewPoints::NewPoints(const vector<Vector2d> vertex, const vector<Vector2d> intersectionPoints, const Vector2d s1, const Vector2d s2)
{
    _vertex = vertex;
    _intersectionPoints = intersectionPoints;
    _s1 = s1;
    _s2 = s2;
}

vector<Vector2d> NewPoints::OrderedVertex()
{
    vector<Vector2d> orderedVertex;
    int primoIndex;
    // ipotizzo che il primo vertice dato sia quello con ascissa e ordinata minima
    double minX = _vertex[0][0];
    double minY = _vertex[0][1];
    int flagX = 0;
    int flagY = 0;
    bool daOrdinare = false;
    for (unsigned int v=1; v<_vertex.size(); v++)
    {
        if (_vertex[v][0] <= minX)
            flagX = v;
        if (_vertex[v][1] <= minY)
            flagY = v;
        // se trovo un vertice che ha sia l'ascissa che l'ordinata minori o uguali del minimo, diventa il nuovo minimo
        if (flagX != 0 && flagY != 0)
        {
            minX = _vertex[v][0];
            minY = _vertex[v][1];
            primoIndex = v; //voglio che questo punto diventi il primo
            flagX=0;
            flagY=0;
            daOrdinare = true; //segno che i vertici non sono già a posto
        }
        else
        {
            flagX=0;
            flagY=0;
        }
    }
    if (daOrdinare == true) //se i vertici non sono già a posto, li sistemo sapendo che l'ordine in cui erano dati va mantenuto (antiorario), ma li faccio solo scorrere
    {
        for (unsigned int a=primoIndex; a<_vertex.size(); a++)
            orderedVertex.push_back(_vertex[a]);
        for (int b=0; b<primoIndex; b++)
            orderedVertex.push_back(_vertex[b]);
    }
    else
        orderedVertex = _vertex;
    return orderedVertex;
}

void NewPoints::CalcPoints()
{
    Reset();
    int numVertex = _vertex.size();
    int numIntersections = _intersectionPoints.size();
    vector<Vector2d> orderedVertex = OrderedVertex(); //metto in ordine i vertici che ricevo in input, in modo che il primo sia quello in basso a sinistra
    for (int i=0; i<numVertex; i++)
      _newPoints.push_back(orderedVertex[i]); // prima inserisco i vertici del poligono

    int flag=0, flag1=0, flag2=0;

    for (int j=0; j<numIntersections; j++) //scorro i punti di intersezione
    {
        for (int i=0; i<numVertex; i++) //scorro i vertici
        {
            //controllo se il punto di intersezione coincide con il vertice a meno di una tolleranza
            if (abs(_intersectionPoints[j](0) - orderedVertex[i](0)) < tolerance && abs(_intersectionPoints[j](1) - orderedVertex[i](1)) < tolerance)
                flag = 1;
        }
        if (flag == 0)
            _newPoints.push_back(_intersectionPoints[j]); //aggiungo il punto a newPoints solo se non è un vertice
        flag = 0;
    }

    for (unsigned int k=0; k<_newPoints.size(); k++)
    {
        //controllo se i due estremi del segmento coincidono con vertici o intersezioni a meno di una tolleranza
        if (abs(_s1(0) - _newPoints[k](0)) < tolerance && abs(_s1(1) - _newPoints[k](1)) < tolerance)
            flag1 = 1;
        if (abs(_s2(0) - _newPoints[k](0)) < tolerance && abs(_s2(1) - _newPoints[k](1)) < tolerance)
            flag2 = 1;
    }
    // se non coincidono con niente, li aggiungo
    if (flag1 == 0)
        _newPoints.push_back(_s1);
    if (flag2 == 0)
        _newPoints.push_back(_s2);
}

}
