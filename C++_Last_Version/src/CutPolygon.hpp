#ifndef CUTPOLYGON_H
#define CUTPOLYGON_H

#include <iostream>
#include <math.h>
#include <vector>
#include <Eigen>
#include <Intersector.hpp>
#include <Segment.hpp>
#include <NewPoints.hpp>

#include <Pol.hpp>

using namespace std;
using namespace Eigen;
using namespace PolLibrary;

namespace PolygonCutLibrary {

  class CutPolygon : public ICutPolygon {

  public:
      static double tolerance;

    private:
      vector<Vector2d> _vertex, _ordVertex;
      Vector2d _s1, _s2;
      vector<bool> _edgeWithInters;
      int _numIntersections = 0;
      Vector2d _intersectionCoordinates;
      vector<Vector2d> _intersectionPoints;
      vector<vector<Vector2d>> _cuttedPolygons;
      vector<vector<int>> _newPolygonsVertex;
      vector<double> _distances;
      vector<Vector2d> _puntiCorrispondenti;

      void Reset();

  public:

      CutPolygon(const vector<Vector2d> vertex, Vector2d& s1, Vector2d& s2);

      void Cut();
      int& NumIntersections() {return _numIntersections;}
      vector<Vector2d> IntersectionPoints() const { return _intersectionPoints;}

      void ChooseFunction(); //scelgo se usare la funzione per i poligoni che hanno due intersezioni o l'altra
      void NewPolygonsTwoInt(); //per i convessi e per i concavi con due intersezioni
      void NewPolygonsMoreInt(); //per gli altri concavi, con più intersezioni
      vector<vector<Vector2d>> CuttedPolygons() {return _cuttedPolygons;} //restituisce tutti i nuovi poligoni tramite le coordinate dei punti
      vector<vector<int>> NewPolygonsVertex() {return _newPolygonsVertex;} //restituisce tutti i nuovi poligoni tramite i numeri assegnati ai vertici
  };
}

#endif // CUTPOLYGON_H
