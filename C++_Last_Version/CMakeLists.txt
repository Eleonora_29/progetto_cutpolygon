# Set minimum CMake version
cmake_minimum_required(VERSION 3.10)

# Set Project Name and language used
project(PolygonCut LANGUAGES CXX) #come si chiama l'eseguibile che stiamo usando, il compilato va nella cartella debug

# Set C++ standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# Add Google Testing Environment
####################################### Start
add_definitions(-DGTEST_LANGUAGE_CXX11)

find_package(Threads REQUIRED)
include(GoogleTest)

if ($ENV{GOOGLETEST_DIR})
    set(GOOGLETEST_DIR $ENV{GOOGLETEST_DIR})
else ()
    if (NOT ${CMAKE_CURRENT_SOURCE_DIR}/googletest STREQUAL "")
        message(WARNING "Using googletest src dir specified at Qt Creator wizard")
    endif ()
    set(GOOGLETEST_DIR ${CMAKE_CURRENT_SOURCE_DIR}/googletest)
endif ()

if (EXISTS ${GOOGLETEST_DIR})
    set(GTestSrc ${GOOGLETEST_DIR}/googletest)
    set(GMockSrc ${GOOGLETEST_DIR}/googlemock)
elseif (UNIX AND EXISTS /usr/src/gtest)
    set(GTestSrc /usr/src/gtest)
    message(WARNING "Using gtest from system")
    if (EXISTS /usr/src/gmock)
        set(GMockSrc /usr/src/gmock)
    endif ()
else ()
    message( FATAL_ERROR "No googletest src dir found - set GOOGLETEST_DIR to enable!")
endif ()

set(GTestFiles ${GTestSrc}/src/gtest-all.cc)
set(GTestIncludes ${GTestSrc} ${GTestSrc}/include)
if (NOT ${GMockSrc} STREQUAL "")
    list(APPEND GTestFiles ${GMockSrc}/src/gmock-all.cc)
    list(APPEND GTestIncludes ${GMockSrc} ${GMockSrc}/include)
endif ()

include_directories(${GTestIncludes})
####################################### End

# Add source code
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src) #includo la cartella src, ovvero il suo path
# includo i due file della cartella: creo una lista e al suo interno includo i loro path
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/Pol.hpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/NewPoints.cpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/NewPoints.hpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/Segment.cpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/Segment.hpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/CutPolygon.cpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/CutPolygon.hpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/Intersector.cpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/Intersector.hpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/Polygon.cpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/Polygon.hpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/ReferenceElement.cpp)
list(APPEND source_code ${CMAKE_CURRENT_SOURCE_DIR}/src/ReferenceElement.hpp)

# Add Unit Tests code
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/test)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_newPoints.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_segment.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_polygon.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_cutPolygon.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_intersector.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_rectangle.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_pentagon.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_convexPolygon.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_concavePolygon1.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_concavePolygon2.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_concavePolygon3.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_concavePolygon4.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_concavePolygon5.hpp)
list(APPEND test_code ${CMAKE_CURRENT_SOURCE_DIR}/test/test_refElement.hpp)

# Add Eigen Library
include_directories(SYSTEM ${CMAKE_CURRENT_SOURCE_DIR}/eigen/include)

# Build executable
add_executable(${PROJECT_NAME} main.cpp ${source_code} ${test_code} ${GTestFiles}) #creo l'eseguibile e gli dò lo stesso nome del progetto

# Set Unit Tests
target_link_libraries(${PROJECT_NAME} PRIVATE Threads::Threads) #linka le librerie che servono per funzionare
gtest_discover_tests(${PROJECT_NAME})

