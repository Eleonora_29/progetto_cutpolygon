#ifndef __TEST_CONVEXPOLYGON_H
#define __TEST_CONVEXPOLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "CutPolygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace ConvexPolygonTesting {

  TEST(TestPolygonCut, TestConvexPolygon)
  {
      Vector2d v1, v2, v3, v4;
      v1 << 2.4, 1.1;
      v2 << 5.8, 2.6;
      v3 << 3.3, 6.8;
      v4 << 1.7, 6.0;

      Vector2d s1, s2;
      s1 << 2.8, 2.0;
      s2 << 4.4, 3.9;

      vector<Vector2d> convexPolygonVertex;
      convexPolygonVertex.push_back(v1);
      convexPolygonVertex.push_back(v2);
      convexPolygonVertex.push_back(v3);
      convexPolygonVertex.push_back(v4);

      try
      {
        PolygonCutLibrary::CutPolygon(convexPolygonVertex,s1,s2);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      PolygonCutLibrary::CutPolygon convexPolygon(convexPolygonVertex, s1, s2);

      try
      {
        convexPolygon.Cut();
        EXPECT_EQ(2, convexPolygon.NumIntersections());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      Vector2d inters1, inters2, a, b;
      vector<Vector2d> intersectionPoints;
      // i punti di intersezione che mi aspetto
      inters1 << 4.7668701, 4.3356582;
      inters2 << 2.3480916, 1.4633588;

      try
      {
        // i punti di intersezione che calcola
        a = convexPolygon.IntersectionPoints()[0];
        b = convexPolygon.IntersectionPoints()[1];
        intersectionPoints = convexPolygon.IntersectionPoints();
        // la loro differenza deve essere minore di una tolleranza
        EXPECT_TRUE(abs(a(0) - inters1(0)) < 1e-6);
        EXPECT_TRUE(abs(a(1) - inters1(1)) < 1e-6);
        EXPECT_TRUE(abs(b(0) - inters2(0)) < 1e-6);
        EXPECT_TRUE(abs(b(1) - inters2(1)) < 1e-6);
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      try
      {
         PolygonCutLibrary::NewPoints(convexPolygonVertex, intersectionPoints, s1, s2);
      }
      catch (const exception& exception)
      {
         FAIL();
      }

      PolygonCutLibrary:: NewPoints newP(convexPolygonVertex, intersectionPoints, s1, s2);

      vector<Vector2d> newPoints;
      try
      {
        newP.CalcPoints();
        EXPECT_EQ(8, newP.NumNewPoints()); //un'intersezione coincide con un vertice
        newPoints = newP.ReturnPoints();
        // controllo che i primi 4 punti corrispondano ai vertici
        EXPECT_EQ(v1, newPoints[0]);
        EXPECT_EQ(v2, newPoints[1]);
        EXPECT_EQ(v3, newPoints[2]);
        EXPECT_EQ(v4, newPoints[3]);
        // controllo che quarto e quinto siano intersezioni
        EXPECT_EQ(a, newPoints[4]);
        EXPECT_EQ(b, newPoints[5]);
        //controllo che gli ultimi due siano gli estremi del segmento
        EXPECT_EQ(s1, newPoints[6]);
        EXPECT_EQ(s2, newPoints[7]);
        convexPolygon.ChooseFunction();
      }
      catch (const exception& exception)
      {
        FAIL();
      }

      vector<Vector2d> cuttedPolygon1, cuttedPolygon2;
      vector<vector<Vector2d>> allCuttedPol;
      vector<int> newVertex1, newVertex2;
      vector<vector<int>> allNewVertex;

      cuttedPolygon1.push_back(v1);
      cuttedPolygon1.push_back(v2);
      cuttedPolygon1.push_back(a);
      cuttedPolygon1.push_back(s2);
      cuttedPolygon1.push_back(s1);
      cuttedPolygon1.push_back(b);
      newVertex1.push_back(0);
      newVertex1.push_back(1);
      newVertex1.push_back(4);
      newVertex1.push_back(5);
      newVertex1.push_back(6);
      newVertex1.push_back(7);

      cuttedPolygon2.push_back(a);
      cuttedPolygon2.push_back(v3);
      cuttedPolygon2.push_back(v4);
      cuttedPolygon2.push_back(b);
      cuttedPolygon2.push_back(s1);
      cuttedPolygon2.push_back(s2);
      newVertex2.push_back(4);
      newVertex2.push_back(2);
      newVertex2.push_back(3);
      newVertex2.push_back(7);
      newVertex2.push_back(6);
      newVertex2.push_back(5);

      allCuttedPol.push_back(cuttedPolygon1);
      allCuttedPol.push_back(cuttedPolygon2);
      allNewVertex.push_back(newVertex1);
      allNewVertex.push_back(newVertex2);

      try
      {
        EXPECT_EQ(allCuttedPol, convexPolygon.CuttedPolygons());
        EXPECT_EQ(allNewVertex, convexPolygon.NewPolygonsVertex());
      }
      catch (const exception& exception)
      {
        FAIL();
      }

  }
}

#endif // __TEST_CONVEXPOLYGON_H
