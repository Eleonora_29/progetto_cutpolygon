#ifndef __TEST_SEGMENT_H
#define __TEST_SEGMENT_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "NewPoints.hpp"
#include "Segment.hpp"
#include "CutPolygon.hpp"
#include "Intersector.hpp"

using namespace testing;
using namespace std;

namespace SegmentTesting {

  TEST(TestPolygonCut, TestSegment)
  {
    Vector2d s1, s2;
    s1 << 1.0, 5.0;
    s2 << 4.0, 1.0;

    try
    {
         PolygonCutLibrary::Segment(s1, s2);
    }
    catch (const exception& exception)
    {
         FAIL();
    }

    PolygonCutLibrary::Segment segment(s1,s2);

    Vector2d direz;
    direz << 3.0, -4.0;

    try
    {
        EXPECT_EQ(segment.Distance(), 5);
        EXPECT_EQ(segment.Direction(), direz);
    }
    catch (const exception& exception)
    {
        FAIL();
    }

  }
}

#endif // __TEST_SEGMENT_H
