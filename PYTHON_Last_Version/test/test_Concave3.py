from unittest import TestCase
import src.Result as Result
import src.Vector2D as V2d


class TestConcavePolygon3(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(1, 1),
                    V2d.Vector2d(4.5, 0.5),
                    V2d.Vector2d(5, 4.5),
                    V2d.Vector2d(3.5, 3.5),
                    V2d.Vector2d(3, 5),
                    V2d.Vector2d(2, 3.5),
                    V2d.Vector2d(0.5, 5.5)]
        return vertices

    def test_Polygon(self):
        try:
            vertices = TestConcavePolygon3.FillPolygonVertices()
            s1 = V2d.Vector2d(1.1, 3.8)
            s2 = V2d.Vector2d(2.9, 3.9)
            product = Result.Result(vertices, s1, s2)
            Result.Result.add_new_points(product)
            Result.Result.add_new_polygons(product)
            result = Result.Result.return_product(product)
            self.assertEqual(str(result),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(1.0, 1.0), (4.5, 0.5), (5.0, 4.5), (3.5, 3.5), (3.0, 5.0), (2.0, 3.5), (0.5, 5.5),"
                             " (1.1, 3.8), (2.9, 3.9), (4.9392, 4.0133), (4.2091, 3.9727), (3.3582, 3.9255),"
                             " (2.2423, 3.8635), (1.748, 3.836), (0.6914, 3.7773)]\nThe number of new points is:\n15\n"
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(1.0, 1.0), (4.5, 0.5), (4.9392, 4.0133), (4.2091, 3.9727), (3.5, 3.5), "
                             "(3.3582, 3.9255), (2.9, 3.9), (2.2423, 3.8635), (2.0, 3.5), (1.748, 3.836), (1.1, 3.8), "
                             "(0.6914, 3.7773)], [(4.9392, 4.0133), (5.0, 4.5), (4.2091, 3.9727)], [(3.3582, 3.9255), "
                             "(3.0, 5.0), (2.2423, 3.8635), (2.9, 3.9)], [(1.748, 3.836), (0.5, 5.5), (0.6914, 3.7773),"
                             " (1.1, 3.8)])"
                             "\nThe numerical order is:\n([0, 1, 7, 8, 3, 9, 10, 11, 5, 12, 13, 14], [7, 2, 8], "
                             "[9, 4, 11, 10], [12, 6, 14, 13])")
        except:
            self.fail()
