from unittest import TestCase
import src.Result as Result
import src.Vector2D as V2d


class TestPentagon(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(2.5, 1),
                    V2d.Vector2d(4, 2.1),
                    V2d.Vector2d(3.4, 4.2),
                    V2d.Vector2d(1.6, 4.2),
                    V2d.Vector2d(1, 2.1)]
        return vertices

    def test_Polygon_Pent(self):
        try:
            vertices = TestPentagon.FillPolygonVertices()
            s1 = V2d.Vector2d(1.4, 2.75)
            s2 = V2d.Vector2d(3.6, 2.2)
            product = Result.Result(vertices, s1, s2)
            Result.Result.add_new_points(product)
            Result.Result.add_new_polygons(product)
            result = Result.Result.return_product(product)
            self.assertEqual(str(result),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(2.5, 1.0), (4.0, 2.1), (3.4, 4.2), (1.6, 4.2), (1.0, 2.1), (1.4, 2.75), (3.6, 2.2), "
                             "(1.2, 2.8)]\nThe number of new points is:\n8\n"
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(2.5, 1.0), (4.0, 2.1), (3.6, 2.2), (1.4, 2.75), (1.2, 2.8), (1.0, 2.1)], [(4.0, 2.1), "
                             "(3.4, 4.2), (1.6, 4.2), (1.2, 2.8), (1.4, 2.75), (3.6, 2.2)])"
                             "\nThe numerical order is:\n([0, 1, 5, 6, 7, 4],"
                             " [1, 2, 3, 7, 6, 5])")
        except:
            self.fail()
