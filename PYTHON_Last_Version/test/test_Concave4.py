from unittest import TestCase
import src.Result as Result
import src.Vector2D as V2d


class TestConcavePolygon1(TestCase):
    @staticmethod
    def FillPolygonVertices() -> []:
        vertices = [V2d.Vector2d(2, -2),
                    V2d.Vector2d(0, -1),
                    V2d.Vector2d(3, 1),
                    V2d.Vector2d(0, 2),
                    V2d.Vector2d(3, 2),
                    V2d.Vector2d(3, 3),
                    V2d.Vector2d(-1, 3),
                    V2d.Vector2d(-3, 1),
                    V2d.Vector2d(0, 0),
                    V2d.Vector2d(-3, -2)]
        return vertices

    def test_Polygon(self):
        try:
            vertices = TestConcavePolygon1.FillPolygonVertices()
            s1 = V2d.Vector2d(-1, -1)
            s2 = V2d.Vector2d(1, 1)
            product = Result.Result(vertices, s1, s2)
            Result.Result.add_new_points(product)
            Result.Result.add_new_polygons(product)
            result = Result.Result.return_product(product)
            self.assertEqual(str(result),
                             "The set of vertices, endpoints of the segment and intersection points is:\n"
                             "[(-3.0, -2.0), (2.0, -2.0), (0.0, -1.0), (3.0, 1.0), (0.0, 2.0), (3.0, 2.0), (3.0, 3.0), "
                             "(-1.0, 3.0), (-3.0, 1.0), (0.0, 0.0), (-1.0, -1.0), (1.0, 1.0), (-2.0, -2.0), "
                             "(1.5, 1.5), (2.0, 2.0)]\nThe number of new points is:\n15\n"
                             "The coordinates of the vertices of the new polygons are:\n"
                             "([(-3.0, -2.0), (-2.0, -2.0), (-1.0, -1.0), (0.0, 0.0)], [(-2.0, -2.0), (2.0, -2.0), "
                             "(0.0, -1.0), (3.0, 1.0), (1.5, 1.5), (1.0, 1.0), (0.0, 0.0), (-1.0, -1.0)], [(1.5, 1.5), "
                             "(0.0, 2.0), (2.0, 2.0), (3.0, 3.0), (-1.0, 3.0), (-3.0, 1.0), (0.0, 0.0), (1.0, 1.0)], "
                             "[(2.0, 2.0), (3.0, 2.0), (3.0, 3.0)])"
                             "\nThe numerical order is:\n([0, 10, 11, 9], [10, 1, 2, 3, 13, 12, 9, 11], "
                             "[13, 4, 14, 6, 7, 8, 9, 12], [14, 5, 6])")
        except:
            self.fail()
