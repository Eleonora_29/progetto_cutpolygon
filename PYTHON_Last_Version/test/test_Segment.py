from unittest import TestCase
import src.Segment as Seg
import src.Vector2D as V2d


class TestSegment(TestCase):
    def test_to_matrix(self):
        try:
            s1 = V2d.Vector2d(1, 2)
            s2 = V2d.Vector2d(5, 9)
            segment = Seg.Segment(s1, s2)
            self.assertEqual(str(Seg.Segment.to_matrix(segment)), "[[1. 5.]\n [2. 9.]]")
        except:
            self.fail()

    def test_distance(self):
        try:
            s1 = V2d.Vector2d(1, 2)
            s2 = V2d.Vector2d(2, 3)
            segment = Seg.Segment(s1, s2)
            self.assertTrue(abs(Seg.Segment.distance(segment) - 1.414213562) < 1e-6)
        except:
            self.fail()

    def test_direction(self):
        try:
            s1 = V2d.Vector2d(2, 4)
            s2 = V2d.Vector2d(0, 4)
            segment = Seg.Segment(s1, s2)
            self.assertTrue(any(abs(Seg.Segment.direction(segment) - [[-2], [0]])) < 1e-6)
        except:
            self.fail()
