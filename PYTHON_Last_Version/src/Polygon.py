import src.Vector2D as V2d
import numpy as np


class Polygon:
    @staticmethod
    def create_polygon(vertices):
        if str(type(vertices)) == "<class 'numpy.ndarray'>":  # Type control
            return vertices
        else:
            num_vertices = len(vertices)
            vertices_matrix = np.zeros((2, num_vertices), float)
            v = 0
            while v < num_vertices:
                for vector in vertices:  # Inserting the point into a numpy array
                    vertices_matrix[0][v] = V2d.Vector2d.x(vector)
                    vertices_matrix[1][v] = V2d.Vector2d.y(vector)
                    v += 1
            return vertices_matrix
