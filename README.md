## Project
The implementation of a program in the C++ and Python languages is required that performs the following operations:

1. Receive as input the vertices of a polygon and the ends of a cut segment;
2. Return as output the set of points formed by the vertices of the polygon, the extremities of the segment and the points of intersection;
3. Output the set of polygons created after the polygon has been cut;
4. Cover a rectangular domain with *tiles* containing a concave polygon enclosed by a bounding box.
