#ifndef INTERSECTOR_H
#define INTERSECTOR_H

#include <iostream>
#include <vector>
#include <Eigen>
#include "Point.hpp"

using namespace std;
using namespace Eigen;

namespace PolygonCutLibrary {

  class Intersector {
    public:
        Intersector();
        ~Intersector();

        void SetEdge(const Point& origin, const Point& end) { }
        void SetStraight(const Point& origin, const double& coeff) { }
        bool ComputeIntersection();
        const Vector2d& ParametricCoordinates() {}
        const double& FirstParametricCoordinate() {}
        const double& SecondParametricCoordinate() {}

    // qui bisogna considerare l'intersezione tra i lati del poligono e la retta (prolungamento del segmento)
  };
}

#endif // INTERSECTOR_H
