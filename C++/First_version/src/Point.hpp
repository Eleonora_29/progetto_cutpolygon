#ifndef POINT_H
#define POINT_H

using namespace std;

namespace PolygonCutLibrary {

  class Point {
    public:
      double _x;
      double _y;
      Point(const double& x=0,
            const double& y=0);
      ~Point();

      static double Distance (const Point& X, const Point& Y);
  };
}

#endif // POINT_H
