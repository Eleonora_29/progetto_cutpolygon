#ifndef __TEST_EMPTYCLASS_H
#define __TEST_EMPTYCLASS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Point.hpp"
#include "Segment.hpp"
#include "Polygon.hpp"
#include "Intersector.hpp"

using namespace PolygonCutLibrary;
using namespace testing;
using namespace std;

namespace RectangleTesting {

  TEST(TestPolygonCut, TestRectangle)
  {

      PolygonCutLibrary::Point v1 = PolygonCutLibrary::Point(1.0, 1.0);
      PolygonCutLibrary::Point v2 = PolygonCutLibrary::Point(5.0, 1.0);
      PolygonCutLibrary::Point v3 = PolygonCutLibrary::Point(5.0, 3.1);
      PolygonCutLibrary::Point v4 = PolygonCutLibrary::Point(1.0, 3.1);

      PolygonCutLibrary::Point s1 = PolygonCutLibrary::Point(2.0, 1.2);
      PolygonCutLibrary::Point s2 = PolygonCutLibrary::Point(4.0, 3.0);

    //inserisco i vertici in un vettore di punti

    vector<Point *> rectangle;
    rectangle[0]= &v1;
    rectangle[1]= &v2;
    rectangle[2]= &v3;
    rectangle[3]= &v4;

    try
    {
          PolygonCutLibrary::Polygon(4,rectangle);
    }
    catch (const exception& exception)
    {
          FAIL();
    }

    //controllo se c'è intersezione con i 4 lati del rettangolo

    Intersector intersector1;
    Intersector intersector2;
    Intersector intersector3;
    Intersector intersector4;

    intersector1.SetEdge(v1, v2);
    intersector1.SetStraight(s1, 0.9);
    EXPECT_TRUE(intersector1.ComputeIntersection()); //interseca
    EXPECT_FLOAT_EQ(1.78, intersector1.FirstParametricCoordinate()); //è approssimato
    EXPECT_FLOAT_EQ(1.0, intersector1.SecondParametricCoordinate());

    intersector2.SetEdge(v2, v3); //non interseca
    intersector2.SetStraight(s1, 0.9);
    EXPECT_TRUE(!intersector2.ComputeIntersection());

    intersector3.SetEdge(v3, v4);
    intersector3.SetStraight(s1, 0.9);
    EXPECT_TRUE(intersector3.ComputeIntersection());
    EXPECT_FLOAT_EQ(4.11, intersector3.FirstParametricCoordinate()); //è approssimato
    EXPECT_FLOAT_EQ(3.1, intersector3.SecondParametricCoordinate());

    intersector4.SetEdge(v4, v1);
    intersector4.SetStraight(s1, 0.9);
    EXPECT_TRUE(!intersector4.ComputeIntersection());

    vector<Point *> newPoints;
    // bisogna controllare che restituisca in imput tutti i punti?
    // bisogna controllare che costruisca nuovi poligoni

  }
}

#endif // __TEST_RECTANGLE_H
